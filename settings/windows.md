# Windows

## winget

Since 2021 Windows 10/11 we can use [winget](https://github.com/microsoft/winget-cli/releases) instead chocolatey.
If you do not want to login to the microsoft store, you will need to use version 1.3 or higher

``` powershell
winget install Google.Chrome
winget install 7zip.7zip
winget install Notepad++.Notepad++
winget install Microsoft.WindowsTerminal
winget install Git.Git
winget install Python.Python.3
winget install LLVM.LLVM
winget install WinMerge.WinMerge
winget install Microsoft.VisualStudioCode
winget install Microsoft.VisualStudio.2022.Community
```

## Chocolatey

Install a package manager for windows.

``` bash
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

## Developer tools

Install a set of developer tools

``` bash
choco install googlechrome
choco install 7zip
choco install notepadplusplus
choco install microsoft-windows-terminal
choco install git
choco install python --version=3.7
choco install llvm --version=10
choco install winmerge
choco install vscode
choco install visualstudio2019community
```

## Enable ssh configuration

[Enable ssh configuration](https://docs.microsoft.com/es-es/windows-server/administration/openssh/openssh_install_firstuse).

``` powershell
Add-WindowsCapability -Online -Name OpenSSH.Client*
Add-WindowsCapability -Online -Name OpenSSH.Server*
Set-Service -Name ssh-agent -StartupType Automatic
Set-Service -Name sshd -StartupType Automatic
Start-Service ssh-agent; Start-Service sshd

ssh-add.exe ./id_rsa
```
